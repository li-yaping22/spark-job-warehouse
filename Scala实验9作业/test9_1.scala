CREATE DATABASE IF NOT EXISTS sparksql_practice;
USE sparksql_practice;
CREATE TABLE IF NOT EXISTS students (
    student_id INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    age INT,
    major VARCHAR(100),
    enrollment_date DATE
);

INSERT INTO sparksql_practice.students (first_name, last_name, age, major, enrollment_date)
VALUES 
('Bob', 'Johnson', 21, 'Electrical Engineering', '2019-08-7'),
('Carol', 'Williams', 23, 'Data Science', '2021-01-15'),
('David', 'Brown', 25, 'Computer Science', '2018-09-6'),
('Eva', 'Davis', 20, 'Mathematics', '2022-02-12');	