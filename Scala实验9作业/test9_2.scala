import org.apache.spark.sql.SparkSession
object DataInsertion {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder.appName("SparkSQL-DataInsertion").master("local[*]").getOrCreate()    
    // 示例数据
    val data = Seq(
      ("Alice", "Smith", 22, "Computer Science", "2020-09-01"),
      // 添加更多记录...
    ).toDF("first_name", "last_name", "age", "major", "enrollment_date")
    // 写入MySQL
    data.write
      .format("jdbc")
      .option("url", "jdbc:mysql://localhost:3306/sparksql_practice")
      .option("dbtable", "students")
      .option("user", "your_username")
      .option("password", "your_password")
      .option("driver", "com.mysql.cj.jdbc.Driver")
      .mode("append")
      .save()
      
    spark.stop()
  }
}