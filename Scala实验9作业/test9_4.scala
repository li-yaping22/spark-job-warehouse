// 更新一名特定学生的专业为“Data Science”
val studentId = 1 // 假设学生ID为1
spark.sql(s"UPDATE students SET major = 'Data Science' WHERE student_id = $studentId")

// 增加所有学生年龄1岁
spark.sql("UPDATE students SET age = age + 1")

// 验证更新结果
val updatedStudents = spark.sql("SELECT * FROM students")
updatedStudents.show()