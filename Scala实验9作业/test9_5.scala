// 删除所有年龄超过30岁的学生记录
spark.sql("DELETE FROM students WHERE age > 30")

// 假设要删除的学生ID为2
val studentIdToDelete = 2
// 删除特定ID的学生记录
spark.sql(s"DELETE FROM students WHERE student_id = $studentIdToDelete")

// 验证删除成功
val remainingStudents = spark.sql("SELECT * FROM students")
remainingStudents.show()