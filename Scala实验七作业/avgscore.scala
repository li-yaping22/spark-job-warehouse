import org.apache.spark.SparkConf
import org.apache.spark.SparkContext

object AverageScore {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("Average Score").setMaster("local")
    val sc = new SparkContext(conf)
    
    val algorithmScores = sc.textFile("Algorithm.txt")
    val databaseScores = sc.textFile("Database.txt")
    val pythonScores = sc.textFile("Python.txt")
    
    val allScores = algorithmScores.union(databaseScores).union(pythonScores)
    
    val studentScores = allScores.map(line => {
      val fields = line.split(" ")
      (fields(0), fields(1).toDouble)
    })
    
    val averageScores = studentScores
      .groupByKey()
      .mapValues(scores => scores.sum / scores.size)
      .sortByKey()
    
    averageScores.saveAsTextFile("AverageScores.txt")
    
    sc.stop()
  }
}
