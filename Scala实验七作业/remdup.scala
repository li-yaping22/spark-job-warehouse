import org.apache.spark.SparkConf
import org.apache.spark.SparkContext

object RemDup {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("Merge Files").setMaster("local")
    val sc = new SparkContext(conf)
    
    val fileA = sc.textFile("file:///usr/local/spark/sparksqldata/inputFileA.txt")
    val fileB = sc.textFile("file:///usr/local/spark/sparksqldata/inputFileB.txt")
    
    val mergedData = fileA.union(fileB)
      .map(line => (line.split("\t")(0), line.split("\t")(1)))
      .distinct()
      .sortByKey()
    
    mergedData.saveAsTextFile("outputFileC")
    
    sc.stop()
  }
}
