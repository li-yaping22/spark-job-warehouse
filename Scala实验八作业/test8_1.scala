#实验一：基础DataFrame操作
from pyspark.sql import SparkSession

# 创建SparkSession实例
spark = SparkSession.builder \
    .appName("salesDataExperiment") \
    .getOrCreate()

# 读取sales_data.csv文件到DataFrame中
df = spark.read.format("csv") \
    .option("header", "true") \
    .load("/usr/local/spark/data/sales_data.csv")

# 展示DataFrame的前5行数据
df.show(5)

## 实验二：DataFrame字段操作
# 选择"OrderID", "Product"和"Quantity"列，并将"Quantity"列重命名为"UnitsSold"
df_selected = df.select("OrderID", "Product", df["Quantity"].alias("UnitsSold"))

# 显示结果
df_selected.show()

## 实验三：过滤与聚合操作

# 筛选出购买数量超过15个的商品订单
df_filtered = df_selected.filter(df_selected["UnitsSold"] > 15)

# 按产品名称排序
df_sorted = df_filtered.orderBy("Product")

# 显示结果
df_sorted.show()

## 实验四：分组聚合
from pyspark.sql.functions import sum

# 计算每种产品的总销售数量和总销售额
df_grouped = df_selected.groupBy("Product") \
    .agg(sum("UnitsSold").alias("TotalUnitsSold"), 
         sum(df["UnitsSold"] * df["Price"]).alias("TotalSales"))

# 显示结果
df_grouped.show()