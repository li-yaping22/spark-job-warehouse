## 实验五：JOIN操作
from pyspark.sql import SparkSession

# 创建SparkSession实例
spark = SparkSession.builder \
    .appName("salesDataExperiment") \
    .getOrCreate()
    
# 读取product_info.csv文件到DataFrame中
df_product_info = spark.read.format("csv") \
    .option("header", "true") \
    .load("/usr/local/spark/data/product_info.csv")

# 注册为临时表
df_product_info.createOrReplaceTempView("product_info")

# 将sales_data.csv也注册为临时表
df_selected.createOrReplaceTempView("sales_data")

# 进行JOIN操作并展示结果
result = spark.sql("SELECT p.Product, p.Category, SUM(s.UnitsSold) AS TotalUnitsSold \
                    FROM sales_data s \
                    JOIN product_info p ON s.Product = p.Product \
                    GROUP BY p.Product, p.Category")

result.show()