#实验六：从RDD转换至DataFrame并进行数据分析
from pyspark.sql import SparkSession
from pyspark.sql import Row

#步骤一：创建并加载数据

# 创建SparkSession实例
spark = SparkSession.builder.appName("EmployeeDataAnalysis").getOrCreate()
# 读取employee_data.txt文本文件，创建一个RDD
rdd = spark.sparkContext.textFile("/usr/local/spark/data/employee_data.txt")
# 显示原始RDD的前几行数据
rdd.take(5)

#步骤二：将RDD转换为DataFrame

# 将原始文本RDD转换为DataFrame
header = rdd.first()
rdd = rdd.filter(lambda line: line != header)  # 去除首行作为表头
rdd = rdd.map(lambda line: line.split(","))
df = rdd.map(lambda x: Row(id=int(x[0]), name=x[1], age=int(x[2]), department=x[3])).toDF()
# 显示转换后的DataFrame的表结构和前几行数据
df.printSchema()
df.show(5)

#步骤三：DataFrame操作
from pyspark.sql.functions import col, to_date
from pyspark.sql.functions import avg, count
# 将“入职日期”列转换为日期类型
df = df.withColumn("hire_date", to_date(col("hire_date"), "yyyy-MM-dd"))
# 计算公司员工的平均年龄
avg_age = df.select(avg("age")).first()[0]
print("公司员工的平均年龄:", avg_age)
# 按性别分组统计员工数量
gender_count = df.groupBy("gender").agg(count("*").alias("count"))
gender_count.show()