def averageOfListBelowN(list: List[Int], n: Int): Double = {
  val filteredList = list.filter(_ < n)
  if (filteredList.isEmpty) {
    0
  } else {
    filteredList.sum.toDouble / filteredList.length.toDouble
  }
}