def isPalindrome(str: String): Boolean = {
  val len = str.length
  val halfLen = len / 2
  for (i <- 0 until halfLen) {
    if (str(i) != str(len - i - 1)) {
      return false
    }
  }
  true
}

// 测试样例
println(isPalindrome("racecar")) // true
println(isPalindrome("hello")) // false