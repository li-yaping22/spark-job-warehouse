def transformList(list: List[Int]): List[Int] = {
  list.map(x => if (x % 2 == 0) x / 2 else x + 1)
}