trait Logger {
  def log(message: String): Unit = println(message)
}

class Calculator extends Logger {
  def add(x: Int, y: Int): Int = {
    val result = x + y
    log(s"Adding $x and $y. Result: $result")
    result
  }

  def subtract(x: Int, y: Int): Int = {
    val result = x - y
    log(s"Subtracting $y from $x. Result: $result")
    result
  }
}
