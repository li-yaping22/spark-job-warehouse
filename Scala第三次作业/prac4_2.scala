class Calculator {
  def calculate(x: Int, y: Int, operator: Char): Any = {
    operator match {
      case '+' => x + y
      case '-' => x - y
      case '*' => x * y
      case '/' => x / y
      case _ => "Invalid operator"
    }
  }
}
