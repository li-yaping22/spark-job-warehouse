def addOne(list: List[Int]): List[Int] = list.map(_ + 1)

def filterEven(list: List[Int]): List[Int] = list.filter(_ % 2 == 0)

def mapReduce(list: List[Int])(mapFunc: Int => Int)(reduceFunc: (Int, Int) => Int): Int = {
  list.map(mapFunc).reduce(reduceFunc)
}
