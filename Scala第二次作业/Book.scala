class Book(val title: String, val author: String, val isbn: String) {
  var isBorrowed: Boolean = false

  def borrowBook(): Unit = {
    if (!isBorrowed) {
      isBorrowed = true
      println(s"Book $title by $author with ISBN $isbn has been borrowed.")
    } else {
      println(s"Book $title is already borrowed.")
    }
  }

  def returnBook(): Unit = {
    if (isBorrowed) {
      isBorrowed = false
      println(s"Book $title by $author with ISBN $isbn has been returned.")
    } else {
      println(s"Book $title is not currently borrowed.")
    }
  }
}

class Library {
  var availableBooks: List[Book] = List()
  var borrowedBooks: List[Book] = List()

  def addBook(book: Book): Unit = {
    availableBooks = book :: availableBooks
    println(s"Book $book added to the library.")
  }

  def borrowBook(isbn: String): Unit = {
    availableBooks.find(_.isbn == isbn) match {
      case Some(book) =>
        book.borrowBook()
        borrowedBooks = book :: borrowedBooks
        availableBooks = availableBooks.filterNot(_.isbn == isbn)
      case None =>
        println("Book not found in the library.")
    }
  }

  def returnBook(isbn: String): Unit = {
    borrowedBooks.find(_.isbn == isbn) match {
      case Some(book) =>
        book.returnBook()
        availableBooks = book :: availableBooks
        borrowedBooks = borrowedBooks.filterNot(_.isbn == isbn)
      case None =>
        println("Book with specified ISBN not found in borrowed list.")
    }
  }
}

object Library {
  def apply(): Library = new Library()

  def unapply(library: Library): Option[(List[Book], List[Book])] = {
    Some((library.availableBooks, library.borrowedBooks))
  }
}
