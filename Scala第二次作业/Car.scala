class Car(val licensePlate: String, val brand: String, val color: String, val price: Double) {
  def drive(): Unit = {
    println("Car is being driven.")
  }

  def park(): Unit = {
    println("Car is parked.")
  }

  def refuel(): Unit = {
    println("Car is refueled.")
  }
}

class ElectricCar(licensePlate: String, brand: String, color: String, price: Double, val batteryCapacity: Double, var range: Double) extends Car(licensePlate, brand, color, price) {
  def chargeBattery(): Unit = {
    println("Electric car battery is being charged.")
  }

  override def drive(): Unit = {
    if (range > 0) {
      println("Electric car is being driven.")
      range -= 10 // Assuming 10 miles are consumed per drive
    } else {
      println("Electric car cannot be driven, battery needs to be charged.")
    }
  }
}

val electricCar = new ElectricCar("ABC123", "Tesla", "Red", 50000.0, 75.0, 200.0)
electricCar.drive() // Output: Electric car is being driven.
electricCar.drive() // Output: Electric car is being driven.
electricCar.drive() // Output: Electric car is being driven.
electricCar.drive() // Output: Electric car cannot be driven, battery needs to be charged.

