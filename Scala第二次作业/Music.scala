import scala.collection.mutable.ListBuffer
import scala.util.Random

// 播放模式枚举
object PlayMode extends Enumeration {
  type PlayMode = Value
  val Normal, Repeat, Shuffle = Value
}

class MusicPlayer {
  private var musicList: ListBuffer[String] = ListBuffer()
  private var currentMusic: Option[String] = None
  private var playMode: PlayMode.PlayMode = PlayMode.Normal

  // 添加音乐到音乐列表
  def addMusic(music: String): Unit = {
    musicList += music
    println(s"Added music: $music")
  }

  // 从音乐列表中删除音乐
  def deleteMusic(music: String): Unit = {
    if (musicList.contains(music)) {
      musicList -= music
      println(s"Deleted music: $music")
    } else {
      println(s"Music $music not found in the playlist.")
    }
  }

  // 播放音乐
  def playMusic(): Unit = {
    playMode match {
      case PlayMode.Normal =>
        currentMusic = musicList.headOption
      case PlayMode.Repeat =>
        currentMusic = currentMusic.orElse(musicList.headOption)
      case PlayMode.Shuffle =>
        currentMusic = musicList.headOption
        musicList = Random.shuffle(musicList)
    }

    currentMusic match {
      case Some(music) =>
        println(s"Playing music: $music")
      case None =>
        println("No music to play.")
    }
  }

  // 设置播放模式
  def setPlayMode(mode: PlayMode.PlayMode): Unit = {
    playMode = mode
    println(s"Set play mode to: $mode")
  }
}


